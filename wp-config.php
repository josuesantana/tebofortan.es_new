<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tebobdsw' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'n<w#PZaw#1<^TC-!la`-UhU]y*i:b<{YIJ2XF>#o&G`5Gs$B:&(>u]rAvF^lR4yo' );
define( 'SECURE_AUTH_KEY',  'c1kN6BgZQT$UN .=>L(emN:rOCu2L5|E`S^VZAWx%#5h`roN>[O_ut|4u7<z<~ N' );
define( 'LOGGED_IN_KEY',    '8L+?i0N4ASm0S*K?xbcc;?J#$_VqY_,!m1tP|ojP(;0J*faQM]mqd7*S5uensssv' );
define( 'NONCE_KEY',        'HxW{.#{6?Mxuw=`Q>A#Pf%=3$KwnLg~UZ&jhaT_[Mmsd_k0dMK9j_:obTzCd*.*6' );
define( 'AUTH_SALT',        'g[v6@gw?# |dm_C#h~1Ge*x(#+GbAB?dn3_Q4s@vuOD!C(Lz::#9+cExYeOT%uzs' );
define( 'SECURE_AUTH_SALT', 'dWy}@F]0/7|tsYc{=akY$K.*EhqgDkgeILB*Qs~qHWK_#pKv4P7Bvv^p2t4 VeMp' );
define( 'LOGGED_IN_SALT',   'Qv?R1XfZ@tYj5{Q6Rt#v=c.a2:zwC,2.#{ @BUA!I/<ta`Y{>>e+ b-&W-LL]+1e' );
define( 'NONCE_SALT',       '-dR7|<z+~Wt7)WdmI>6VHIVsf`dpw8+ [%-GrJ2@sxixM2<5]l}TTf~2mVi5|:>M' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
